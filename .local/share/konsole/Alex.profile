[Appearance]
ColorScheme=Alex
Font=Source Code Pro,11,-1,5,50,0,0,0,0,0,Regular

[General]
LocalTabTitleFormat=%#. %u:%D
Name=Alex
Parent=FALLBACK/
RemoteTabTitleFormat=%#. %U%h
ShowTerminalSizeHint=false

[Interaction Options]
AutoCopySelectedText=true
MiddleClickPasteMode=1

[Scrolling]
HistorySize=5000
