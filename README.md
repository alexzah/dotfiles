# Dotfiles

This repository contains dotfiles for various programs on my Linux desktop.

## Bootstrap Process

```
git clone https://gitlab.com/alexzah/dotfiles.git temp-config
bash ./temp-config/.config/bootstrap_arch.sh
rm -rf temp-config
```
