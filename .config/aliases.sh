if command -v exa > /dev/null;
then
  alias ls="exa --classify"
else
    echo "Missing exa! Using ls"
    alias ls="ls --color --classify --human-readable"
fi

alias grep="grep --color"
alias lfd="du -Sh | sort -rh"
alias lf="find -type f -exec du -Sh {} + | sort -rh"
alias rs="rsync --archive --partial --progress"
alias rsd="rsync --archive --partial --progress --dry-run"
alias upd="yay -Syyu --answerclean All --answerdiff All"

# git aliases
alias ga="git add"
alias gb="git branch"
alias gcm="git commit -m"
alias gco="git checkout"
alias gcl="git clone"
alias gd="git diff"
alias gds="git diff --staged"
alias gl="git log"
alias gpu="git push"
alias gpo="git push origin"
alias gpl="git pull"
alias gr="git reset"
alias gst="git status"
alias gstu="git status -uno"

# gitconf aliases for managing dotfiles
alias gitconf='git --git-dir=$HOME/.bare-config/ --work-tree=$HOME'
alias gca="gitconf add"
alias gcb="gitconf branch"
alias gccm="gitconf commit -m"
alias gcco="gitconf checkout"
alias gccl="gitconf clone"
alias gcd="gitconf diff"
alias gcds="gitconf diff --staged"
alias gclo="gitconf log"
alias gcpu="gitconf push"
alias gcpo="gitconf push origin"
alias gcpl="gitconf pull"
alias gcr="gitconf reset"
alias gcst="gitconf status"
alias gcstu="gitconf status -uno"

alias psef='ps -ef | grep'
alias upd='yay --answerclean All --answerdiff None -Syyu'

# cargo aliases
alias cg='cargo'
alias cgr='cargo build'
alias cgr='cargo run'

function extract {
  if [ -z "$1" ]; then
    echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
  else
    if [ -f $1 ]; then
      case $1 in
        *.tar.bz2)   tar xvjf $1    ;;
        *.tar.gz)    tar xvzf $1    ;;
        *.tar.xz)    tar xvJf $1    ;;
        *.lzma)      unlzma $1      ;;
        *.bz2)       bunzip2 $1     ;;
        *.rar)       unrar x -ad $1 ;;
        *.gz)        gunzip $1      ;;
        *.tar)       tar xvf $1     ;;
        *.tbz2)      tar xvjf $1    ;;
        *.tgz)       tar xvzf $1    ;;
        *.zip)       unzip $1       ;;
        *.Z)         uncompress $1  ;;
        *.7z)        7z x $1        ;;
        *.xz)        unxz $1        ;;
        *.exe)       cabextract $1  ;;
        *)           echo "extract: '$1' - unknown archive method" ;;
      esac
    else
      echo "$1 - file does not exist"
    fi
  fi
}

function split_stream()
{
    base_name=${1::-4}
    ffmpeg -i ${base_name}.mkv -map 0:0 -c copy ${base_name}.mp4
    ffmpeg -i ${base_name}.mkv -map 0:2 -c copy ${base_name}_desktop.m4a
    ffmpeg -i ${base_name}.mkv -map 0:3 -c copy ${base_name}_mic.m4a
}
