#!/bin/bash

# Clone the bare repo to get configs
cd ~
git clone --bare https://gitlab.com/alexzah/dotfiles.git $HOME/.bare-config
shopt -s expand_aliases
alias gitbare="/usr/bin/git --git-dir=$HOME/.bare-config/ --work-tree=$HOME"
gitbare checkout -f
gitbare config --local status.showUntrackedFiles no
shopt -u expand_aliases

# Install keyring to avoid any GPG issues
sudo pacman -S archlinux-keyring

# Get yay for installing the package list
sudo pacman -S --needed git base-devel
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ~

# Install the core packages
yay --noconfirm --answerclean All --answerdiff None -S $(cat .config/package_list_core)

# Change shell to zsh
chsh -s /usr/bin/zsh

# Enable sddm
sudo systemctl enable sddm

echo "Bootstrap done. Reboot to get into sddm"
echo "You may need the following packages as well: $(cat .config/package_list_extras)"
