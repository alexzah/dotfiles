#!/bin/bash
gsettings set org.gnome.desktop.interface gtk-theme 'Breeze-Dark'
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
gsettings set org.gnome.desktop.interface cursor-theme 'Breeze'
gsettings set org.gnome.desktop.interface font-name 'sans'
export GTK_THEME="Breeze-Dark"

/usr/lib/xfce4/notifyd/xfce4-notifyd &
xfce4-power-manager &
nm-applet --indicator &
xfce4-volumed-pulse &
mpris-proxy &
lxsession &
source ~/.screenlayout/docked.sh

if [ "$XDG_SESSION_TYPE" = "wayland" ]
then
    udiskie --notify --no-automount --tray --appindicator &
    wlsunset -l 40.7 -L -74 -t 3500 &
    trayer --widthtype request --expand True --align right --margin 110 &
else
    picom --experimental-backends --config /home/alex/.config/picom.conf &  
    blueberry-tray &
    setxkbmap -layout us -option caps:backspace
    xmodmap -e "clear Lock"
    udiskie --notify --no-automount --tray &
    /usr/lib/geoclue-2.0/demos/agent &
    redshift-gtk &
fi

