#!/bin/bash

result=$(echo -e " Shutdown\n Restart\n Suspend" | rofi -dmenu -font "SauceCodePro Nerd Font 12")

if echo "$result" | grep Shutdown; then
    dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.PowerOff" boolean:true
fi

if echo "$result" | grep Restart; then
    dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.Reboot" boolean:true
fi

if echo "$result" | grep Suspend; then
    dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.Suspend" boolean:true
fi
