
# Path to your oh-my-zsh installation.
export ZSH="/usr/share/oh-my-zsh"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    history-substring-search
    colored-man-pages
)

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

source $ZSH/oh-my-zsh.sh

function setup_terminal_customization()
{
	# Set window title
	export PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD}\007"'
}

function customize()
{
  source .config/aliases.sh
  setup_terminal_customization
  source .config/exports.sh
  if [ -f "$HOME/.cargo/env" ]; then
  	source "$HOME/.cargo/env"
  fi
}

precmd() { eval "$PROMPT_COMMAND" }

customize
