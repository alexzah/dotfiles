# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
PATH="$HOME/.local/bin:$HOME/bin:$PATH"
PATH="$HOME/.cargo/bin:$PATH"
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
function setup_terminal_customization()
{
	# Highlight prompt to make call boundaries easier to see
  export PS1='\[\e[1;39;100m\][\u@\h \W]\$ \[\e[0m\]'

	# Set window title
	export PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD}\007"'
}

function customize()
{
  source .config/aliases.sh
  setup_terminal_customization
  source .config/exports.sh
}

customize
